# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Description: A program to make a graph of most used words in a text body
# NOTE: Part of Exercise 2
import nltk
from nltk.book import text1
from nltk import FreqDist

def question_2():
    fdist1 = FreqDist(text1)
    fdist1.most_common(50)
    fdist1.plot(50)

def main():
    question_2()

if __name__ == "__main__":
    main()
