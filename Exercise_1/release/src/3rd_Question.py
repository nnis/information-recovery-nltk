# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Description: A program to make a graph of most used words in a text body
# NOTE: Part of Exercise 3
import nltk
from nltk.book import text6
from nltk import FreqDist

def question_3():
    fdist1 = FreqDist(text6)
    fdist1.most_common(50)
    fdist1.plot(50)

def main():
    question_3()

if __name__ == "__main__":
    main()
