# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-03
# Description: A script to fully compare book coincidence tables with
#              the help of the pandas library
# NOTE: Part of Exercise 3b
import pandas as pd
from nltk.tokenize import word_tokenize
from nltk.book import text4, text7

def question_3b():
    corpus={}
    corpus['text4'] = dict((tok.strip('.'), 1) for tok in text4[0:50])
    corpus['text7'] = dict((tok.strip('.'), 1) for tok in text7[0:50])
    df = pd.DataFrame.from_records(corpus).fillna(0).astype(int).T
    # Show just the first 50 tokens (columns)
    print(df[df.columns[:50]])

def main():
    question_3b()

if __name__ == "__main__":
    main()
