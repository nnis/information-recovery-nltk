# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-03
# Description: A script to find the most used words of the 50 first words of 2
#              books and display them as posting lists.
# NOTE: Part of Exercise 4
import nltk
import string
import numpy as np
import pandas as pd
from nltk import FreqDist
from nltk.stem import PorterStemmer
from nltk.book import text4, text7

def preprocessing(text):
    # Remove punctuations.
    table = str.maketrans('', '', '\t')
    token_list = [word.translate(table) for word in text]
    punctuations = (string.punctuation).replace("'", "")
    trans_table = str.maketrans('', '', punctuations)
    stripped_words = [word.translate(trans_table) for word in token_list]
    token_list = [str for str in stripped_words if str]
    # Change to lowercase.
    token_list =[word.lower() for word in token_list]
    # Get uninteresting words to remove
    nltk.download('stopwords')
    english_stopwords = nltk.corpus.stopwords.words('english')
    # Initialize the stemmer.
    stemmer = PorterStemmer()
    # Remove the stopwords
    for word in token_list:
        if word in english_stopwords:
            token_list.remove(word)
        word = stemmer.stem(word)
    return token_list

def question_4(token_list, text_name,searched_word):
    # Initialize the text no.
    textno = 0
    # Initialize the dictionary.
    pos_index = {}
    # Initialize the text mapping (textno -> text name).
    text_map = {}

    # For position and term in the tokens.
    for pos, term in enumerate(token_list):

        # If term already exists in the positional index dictionary.
        if term in pos_index:
            # Increment total freq by 1.
            pos_index[term][0] = pos_index[term][0] + 1

            # Check if the term has existed in that DocID before.
            if textno in pos_index[term][1]:
                pos_index[term][1][textno].append(pos)
            else:
                pos_index[term][1][textno] = [pos]

        # If term does not exist in the positional index dictionary
        # (first encounter).
        else:
            # Initialize the list.
            pos_index[term] = []
            # The total frequency is 1.
            pos_index[term].append(1)
            # The postings list is initially empty.
            pos_index[term].append({})
            # Add doc ID to postings list.
            pos_index[term][1][textno] = [pos]

    # Map the text no. to the text name.
    text_map[textno] = text_name

    # Increment the text no. counter for document ID mapping
    textno += 1

    # Sample positional index to test the code.
    print(f"We are searching for the word: '{searched_word}'. We found it here:")
    sample_pos_idx = pos_index[searched_word]
    print(" * Positional Index")
    print(f"  {sample_pos_idx}")

    text_list = sample_pos_idx[1]
    print(" * Text name, [Positions]")
    for textno, positions in text_list.items():
        print(f"  {text_map[textno]} {positions}")

def main():
    #Process text to make them better to search and analyse.
    processed_tokens_4 = preprocessing(text4[0:50])
    processed_tokens_7 = preprocessing(text7[0:50])
    # Calculate the frequency of word usage on both texts
    fdist4 = FreqDist(processed_tokens_4)
    fdist7 = FreqDist(processed_tokens_7)
    # Find out the 3 most commons words on both texts
    common_words_4 = fdist4.most_common(3)
    common_words_7 = fdist7.most_common(3)
    print(f"3 most common words in text4 are: \n{common_words_4}")
    print(f"3 most common words in text7 are: \n{common_words_7}")
    question_4(processed_tokens_4,"Inaugural Address Corpus",common_words_4[0][0])
    question_4(processed_tokens_4,"Inaugural Address Corpus",common_words_4[1][0])
    question_4(processed_tokens_4,"Inaugural Address Corpus",common_words_4[2][0])
    question_4(processed_tokens_7,"Wall Street Journal",common_words_7[0][0])
    question_4(processed_tokens_7,"Wall Street Journal",common_words_7[1][0])
    question_4(processed_tokens_7,"Wall Street Journal",common_words_7[2][0])

if __name__ == "__main__":
    main()
