# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-03
# Description: A script to calculate 2 sentences cosine similarity
# NOTE: Part of Exercise 5
import math
import nltk
from nltk.book import text4, text7
from collections import Counter

def cosine_sim(vec1, vec2):
     dot_prod = 0
     for i, v in enumerate(vec1):
        dot_prod += v * vec2[i]
     mag_1 = math.sqrt(sum([x**2 for x in vec1]))
     mag_2 = math.sqrt(sum([x**2 for x in vec2]))
     return dot_prod / (mag_1 * mag_2)

def question_5(tokens):
    document_vector = []
    doc_length = len(tokens)
    nltk.download('stopwords', quiet=True)
    stopwords = nltk.corpus.stopwords.words('english')
    tokens = [x for x in tokens if x not in stopwords]
    counts = Counter(tokens)
    # The "key" variable just to access the value. It is not accessed
    for key,value in counts.most_common():
        document_vector.append(value / doc_length)
    return document_vector

def main():
    vec_text4 = question_5(text4[0:50])
    vec_text7 = question_5(text7[0:50])
    cos_50 = cosine_sim(vec_text4,vec_text7)
    print(f"Cosine similarity of the first 50 words of books 4,7:\n{cos_50}")

if __name__ == "__main__":
    main()
