# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-03
# Description: A script to fully compare sentence coincidence table with
#              the help of the pandas library
# NOTE: Part of Exercise 3a
import nltk
import numpy as np
import pandas as pd
from nltk.tokenize import word_tokenize

def question_3a():
    sentences = """In 1972, Kernighan described memory management in strings
                 using "hello world", in the programming language B,
                 which became the iconic example we know today.&""" \
                """Linux is a clone of the operating system Unix,
                written from scratch by Linus Torvalds with assistance
                from a loosely-knit team of hackers across the Net.
                It aims towards POSIX and Single UNIX Specification
                compliance.&""" \
                """Arch Linux is an independently developed, x86-64
                general-purpose GNU/Linux distribution that strives to
                provide the latest stable versions of most software by
                following a rolling-release model. The default installation
                is a minimal base system, configured by the user to only add
                what is purposely required.& """ \
                """Kernighan's original 1972 implementation of hello, world!
                was sold at The Algorithm Auction, the world's first auction
                of computer algorithms."""
    corpus = {}
    for i, sent in enumerate(sentences.split('&')):
       corpus['sent{}'.format(i + 1)] = dict((tok, 1) for tok in nltk.word_tokenize(sent))
    df = pd.DataFrame.from_records(corpus).fillna(0).astype(int).T
     # Show just the first 13 tokens (columns)
    print(df[df.columns[:13]])

def main():
    question_3a()

if __name__ == "__main__":
    main()
